def html_dict_search(html_dict, selector):
    list = []
    recursionSearch(list, html_dict, selector)
    return list

def recursionSearch(list, section, selector):
    type = "class"
    if(selector[0] == "#"):
        type = "id"
    if(type in (section['attrs']) and (section['attrs'])[type] == selector[1:]):
        list.append(section)
        return
    for sub_section in section['children']:
        recursionSearch(list, sub_section, selector)

        
        

def word_count(s):
    """
    Find the number of occurrences of each word
    in a string(Don't consider punctuation characters)
    """
    dict = {}
    word = ""
    for x in range(0,len(s)):
        if((ord(s[x])>=65 and ord(s[x])<=90) or (ord(s[x])>=97 and ord(s[x])<=122)or(s[x]=="-")):    
            word = word+s[x]
            if(x==len(s)-1):
                if word in dict:
                    dict[word]+= 1
                else:
                    dict[word] = 1  
                word = ""
                break
        elif(word!=""):
            if word in dict:
                dict[word]+= 1
            else:
                dict[word] = 1 
            word = ""
           

    return dict

def dict_items(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    """
    list = []
    for x in d:
        tup = (x,d[x])
        list.append(tup)
    return list


def dict_items_sorted(d):
    """
    Return a list of all the items - (key, value) pair tuples - of the dictionary, `d`
    sorted by `d`'s keys
    """
    list = []
    for key in sorted(d.keys()):
        tup = (key,d[key])
        list.append(tup)
    return list


def sum_items_in_list(x):
    sum=0
    for item in x:
        sum+=item
    return sum


def list_length(x):
    return len(x)

def last_three_items(x):
    return x[-3:]


def first_three_items(x):
    return x[:3]


def sort_list(x):
    return sorted(x)


def append_item(x, item):
    return x+[item]


def remove_last_item(x):
    x.pop()
    return x


def count_occurrences(x, item):
    count=0
    for val in x:
        if item==val:
            count+= 1
    return count

def is_item_present_in_list(x, item):
    return item in x


def append_all_items_of_y_to_x(x, y):
    """
    x and y are lists
    """
    x.extend(y)
    return x


def list_copy(x):
    """
    Create a shallow copy of x
    """
    y=x[:]
    return y

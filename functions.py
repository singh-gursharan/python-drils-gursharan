def is_prime(n):
    """
    Check whether a number is prime or not
    """

    if(n==1):
        return False
    for x in range(2,n//2+1):
        if(n%x==0):
            return False
    return True

def n_digit_primes(digit=2):
    """
    Return a list of `n_digit` primes using the `is_prime` function.

    Set the default value of the `digit` argument to 2
    """
    list=[]
    last=1
    for x in range(0,digit):
        last*=10
    for n in range(last//10,last):
        if(is_prime(n)):
            list+=[n]
    return list

print(n_digit_primes(4))
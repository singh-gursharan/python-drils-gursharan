"""
Implement the below file operations.
Close the file after each operation.
"""


def read_file(path):
    fr= open(path,'r')
    text=fr.read()
    fr.close()
    return text

def write_to_file(path, s):
    fw= open(path,'w')
    fw.write(s)
    fw.close()

def append_to_file(path, s):
    f= open(path,'a')
    f.write(s)
    f.close();

def numbers_and_squares(n, file_path):
    """
    Save the first `n` natural numbers and their squares into a file in the csv format.

    Example file content for `n=3`:

    1,1
    2,4
    3,9
    """
    s=""
    for x in range(1,n+1):
        sq=x**2
        s+=str(x)+","+str(sq)+"\n"
    fw=open(file_path,'w')
    fw.write(s)


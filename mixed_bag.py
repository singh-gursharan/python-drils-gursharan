import os
import random
def unique(items):
    """
    Return a set of unique values belonging to the `items` list
    """
    set1=set()
    for x in items:
        set1.add(x)
    return set1


def shuffle(items):
    """
    Shuffle all items in a list
    """
    length = len(items)
    for i in range(0,length):
        j= random.randint(i,length-1)
        temp = items[i]
        items[i] = items[j]
        items[j] = temp
    return items

def getcwd():
    """
    Get current working directory
    """
    print(os.getcwd()) 


def mkdir(name):
    """
    Create a directory at the current working directory
    """
    os.mkdir(name);


print(shuffle([1,2,3,4,5,6,7]))